The easiest way to run this project:

in the root folder of the project, in a terminal run:

`java -jar PatternCounter.jar Input.txt 1`

>![input1](./doc/input1.png)


`java -jar PatternCounter.jar Input.txt 2`

>![input1](./doc/input2.png)

`java -jar PatternCounter.jar Input.txt 3`

>![input1](./doc/input3.png)

Also, you can supply your own txt file to match the pattern that you want.

using *mostly **streams*** because they are multithreaded, also I know sometimes can be little confused

The **MainTest** class is the one who owns the integration test of the app.

![tests](./doc/tests.png)

About:
I'm [Miguel R.](https://miguel.debloat.us/) this is my [LinkedIn](https://www.linkedin.com/in/rmigue) profile and 
my [github](https://www.github.com/miguebarbell) profile
