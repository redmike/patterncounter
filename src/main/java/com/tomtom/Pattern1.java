package com.tomtom;

import java.io.FileNotFoundException;
import java.util.Map;
import java.util.stream.Collectors;

public class Pattern1 extends ReaderImpl implements Pattern{
	public Pattern1(String fileName) throws FileNotFoundException {
		super(fileName);
	}

	public Map<String, Long> match() {
		String pattern = ".*[a-zA-Z].*";
		return count(words).keySet().stream().filter(key -> key.matches(pattern))
				.collect(Collectors.toMap(key -> key, key -> count(words).get(key)));
	}

	@Override
	public void prettifiedMatch() {
		match().keySet().forEach(key -> System.out.println(key + ", " + match().get(key)));
	}
}
