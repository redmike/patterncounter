package com.tomtom;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class ReaderImpl implements Reader {
	ArrayList<String> words = new ArrayList<>();
	@Override
	public void getFile(String fileName) {
		try {

			File file = new File(fileName);
			Scanner in = new Scanner(file);
			while (in.hasNext()) {
				String line = in.next();
				words.add(line.trim());
			}
		} catch (FileNotFoundException e) {
			System.out.println("File not found");
		}
	}
	public ReaderImpl(String fileName) {
		getFile(fileName);
	}

	public ArrayList<String> getWords() {
		return words;
	}
}
