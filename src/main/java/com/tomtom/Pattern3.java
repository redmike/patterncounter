package com.tomtom;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Pattern3 extends ReaderImpl implements Pattern {
	public Pattern3(String filename) throws FileNotFoundException {
		super(filename);
	}

	@Override
	public Map<String, Long> match() {

		if (words.size() < 3) {

			Map<String, Long> map = new HashMap<>();
			map.put(words.stream()
                   .collect(Collectors.joining(" "))
					, 1L);
			return map;
		} else {
			ArrayList<String> phases = (ArrayList<String>) IntStream.range(0, words.size() - 2)
			                                                        .mapToObj(i -> words.get(i) + " " + words.get(i + 1) + " " + words.get(i + 2))
			                                                        .collect(Collectors.toList());
					return count(phases);
		}
	}

	@Override
	public void prettifiedMatch() {
		match().keySet().forEach(key -> System.out.println(key + ", " + match().get(key)));
	}
}
