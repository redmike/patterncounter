package com.tomtom;

import java.io.FileNotFoundException;

public interface Reader {
	void getFile(String fileName) throws FileNotFoundException;
}
