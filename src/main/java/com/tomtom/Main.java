package com.tomtom;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.stream.Collectors;

public class Main {

	public static void main(String[] args) throws FileNotFoundException {
		if (args.length < 2) {
			System.out.println("Usage:\n<filepath> <option>\nie:\nInput.txt 3");
			return;
		}
		String typeOfMatching = args[args.length - 1];
		String filename = Arrays.stream(args).limit(args.length - 1).collect(Collectors.joining(" "));
		switch (typeOfMatching) {
			case "1" -> {
				Pattern1 pattern1 = new Pattern1(filename);
				pattern1.prettifiedMatch();
			}
			case "2" -> {
				Pattern2 pattern2 = new Pattern2(filename);
				pattern2.prettifiedMatch();
			}
			case "3" -> {
				Pattern3 pattern3 = new Pattern3(filename);
				pattern3.prettifiedMatch();
			}
			default -> System.out.println("Invalid type of matching: " + typeOfMatching + "\nOptions: 1, 2 or 3");

		}
	}
}
