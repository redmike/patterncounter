package com.tomtom;

import java.util.ArrayList;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public interface Pattern {
	Map<String, Long> match();
	void prettifiedMatch();

	default Map<String, Long> count(ArrayList<String> words) {
		// https://docs.oracle.com/javase/8/docs/api/java/util/stream/package-summary.html#Ordering
		// Collectors.groupingBy()) can be implemented more efficiently if ordering of elements is not relevant
		// since ordering isn't a requirement, my bias is for performance
		// if we want it to order we can create a linked map and insert every key,value pair
		return words.stream()
		            .collect(Collectors.groupingBy(Function.identity(),
				Collectors.counting()));
	}
}
