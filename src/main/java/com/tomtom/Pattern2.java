package com.tomtom;

import java.io.FileNotFoundException;
import java.util.Map;
import java.util.stream.Collectors;

public class Pattern2 extends ReaderImpl implements Pattern{

	public Pattern2(String fileName) throws FileNotFoundException {
		super(fileName);
	}
	@Override
	public Map<String, Long> match() {
		String pattern = "\\d+";
		return count(words).keySet().stream().filter(key -> key.matches(pattern))
		                   .collect(Collectors.toMap(key -> key, key -> count(words).get(key)));
	}

	@Override
	public void prettifiedMatch() {
		match().keySet().forEach(key -> System.out.println(key + ", " + match().get(key)));
	}
}
