package com.tomtom;

import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.util.Map;


class Pattern2Test {

	Maps maps = new Maps();
	@Test
	public void complexPattern() throws FileNotFoundException {
		Pattern2 complexPattern = new Pattern2("src/test/resources/Complex.txt");
//		complexPattern.prettifiedMatch();
		Map<String, Long> matches = complexPattern.match();

		assert(maps.compareSize(matches, Maps.complexPattern2));
		assert(maps.compareContent(matches, Maps.complexPattern2));
	}

	@Test
	public void inputPattern() throws FileNotFoundException {
		Pattern2 inputPattern = new Pattern2("src/test/resources/Input.txt");
//		inputPattern.prettifiedMatch();
		Map<String, Long> matches = inputPattern.match();
		assert(maps.compareSize(matches, Maps.inputPattern2));
		assert(maps.compareContent(matches, Maps.inputPattern2));
	}
}
