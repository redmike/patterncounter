package com.tomtom;

import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class ReaderImplTest {

	@Test
	void testFileReader() throws FileNotFoundException {

		ReaderImpl reader = new ReaderImpl("src/test/resources/Simple.txt");
		ArrayList<String> expected = new ArrayList<>(
				Arrays.asList("This", "is", "simple", "and", "logic")
		);
		assertEquals(expected, reader.getWords());
	}




}
