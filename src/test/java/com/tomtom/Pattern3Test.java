package com.tomtom;

import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.util.Map;

class Pattern3Test {
	Maps maps = new Maps();
	@Test
		public void inputPattern() throws FileNotFoundException {
		Pattern3 inputPattern = new Pattern3("src/test/resources/Input.txt");
		Map<String, Long> matches = inputPattern.match();
		assert(maps.compareSize(matches, Maps.inputPattern3));
		assert(maps.compareContent(matches, Maps.inputPattern3));
	}

}
