package com.tomtom;

import java.util.HashMap;
import java.util.Map;

public class Maps {

	public static Map<String, Long> simplePattern1 = new HashMap<>() ;
	public static Map<String, Long> inputPattern1 = new HashMap<>() ;
	public static Map<String, Long> complexPattern1 = new HashMap<>() ;
	public static Map<String, Long> inputPattern2 = new HashMap<>() ;
	public static Map<String, Long> complexPattern2 = new HashMap<>() ;
	public static Map<String, Long> inputPattern3 = new HashMap<>() ;

	void inputPattern() {
		inputPattern1.put("surprise", 2l );
		inputPattern1.put("big", 2l);
		inputPattern1.put("a", 2l);
		inputPattern1.put("is", 1l);
		inputPattern1.put("hello", 1l);
		inputPattern2.put("1000", 2l);
		inputPattern2.put("2000", 1l);
		inputPattern3.put("big surprise 1000", 1l);
		inputPattern3.put("2000 hello is", 1l);
		inputPattern3.put("is a big", 1l);
		inputPattern3.put("surprise 2000 hello", 1l);
		inputPattern3.put("big surprise 2000", 1l);
		inputPattern3.put("a big surprise", 2l);
		inputPattern3.put("1000 a big", 1l);
		inputPattern3.put("hello is a", 1l);

	}
	void complexPattern() {

		complexPattern1.put("number", 1l);
		complexPattern1.put("5OO", 1l);
		complexPattern1.put("5oo", 1l);
		complexPattern1.put("in", 1l);
		complexPattern1.put("hav3", 3l);
		complexPattern1.put("num", 1l);
		complexPattern1.put("this", 1l);
		complexPattern1.put("5some", 2l);
		complexPattern1.put("between", 1l);
		complexPattern2.put("200", 2l);
		complexPattern2.put("500", 1l);
	}
	void simplePattern() {
		simplePattern1.put("and", 1l);
		simplePattern1.put("This", 1l);
		simplePattern1.put("simple", 1l);
		simplePattern1.put("is", 1l);
		simplePattern1.put("logic", 1l);
	}
	public Maps() {
		simplePattern();
		inputPattern();
		complexPattern();
	}
	boolean compareSize(Map<String, Long> test, Map<String, Long> expect) {
				return expect.keySet().size() == test.keySet().size();
	}
	boolean compareContent(Map<String, Long> test, Map<String, Long> expect) {
		for (String key : test.keySet()) {
			if (!test.get(key).equals(expect.get(key))) {
				return false;
			}
		}
		return true;
	}
}
