package com.tomtom;

import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.util.Map;

class Pattern1Test {
	Maps maps = new Maps();
	@Test
	public void simplePattern() throws FileNotFoundException {
		Pattern1 simplePattern = new Pattern1("src/test/resources/Simple.txt");
		Map<String, Long> matches = simplePattern.match();

		assert(maps.compareSize(matches, Maps.simplePattern1));
		assert(maps.compareContent(matches, Maps.simplePattern1));
	}
	@Test
	public void complexPattern() throws FileNotFoundException {
		Pattern1 complexPattern = new Pattern1("src/test/resources/Complex.txt");
		Map<String, Long> matches = complexPattern.match();

		assert(maps.compareSize(matches, Maps.complexPattern1));
		assert(maps.compareContent(matches, Maps.complexPattern1));
	}
	@Test
	public void inputPattern() throws FileNotFoundException {
		Pattern1 inputPattern = new Pattern1("src/test/resources/Input.txt");
		Map<String, Long> matches = inputPattern.match();
		assert(maps.compareSize(matches, Maps.inputPattern1));
		assert(maps.compareContent(matches, Maps.inputPattern1));
	}

}
