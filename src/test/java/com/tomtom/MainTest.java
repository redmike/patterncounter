package com.tomtom;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MainTest {

	Maps maps = new Maps();
    private PrintStream originalSystemOut;
    private ByteArrayOutputStream systemOutContent;

    @BeforeEach
    void redirectSystemOutStream() {
        originalSystemOut = System.out;
        systemOutContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(systemOutContent));
    }

    @AfterEach
    void restoreSystemOutStream() {
        System.setOut(originalSystemOut);
    }

	@Test
	void inputTxt2() throws IOException {
		systemOutContent.reset();
		Main.main(new String[]{"src/test/resources/Input.txt", "2"});

		Map<String, Long> matches = Arrays.stream(systemOutContent.toString().split("\n"))
				.collect(Collectors.toMap(
						line -> line.split(",")[0].trim(),
						line -> Long.parseLong(line.split(",")[1].trim())));
		assert(maps.compareSize(matches, Maps.inputPattern2));
		assert(maps.compareContent(matches, Maps.inputPattern2));
	}
	@Test
	void inputTxt1() throws IOException {
		systemOutContent.reset();
		Main.main(new String[]{"src/test/resources/Input.txt", "1"});
		Map<String, Long> matches = Arrays.stream(systemOutContent.toString().split("\n"))
		                                  .collect(Collectors.toMap(
				                                  line -> line.split(",")[0].trim(),
				                                  line -> Long.parseLong(line.split(",")[1].trim())));
		assert(maps.compareSize(matches, Maps.inputPattern1));
		assert(maps.compareContent(matches, Maps.inputPattern1));
	}
	@Test
	void inputTxt3() throws FileNotFoundException {
		systemOutContent.reset();
		Main.main(new String[]{"src/test/resources/Input.txt", "3"});
		Map<String, Long> matches = Arrays.stream(systemOutContent.toString().split("\n"))
		                                  .collect(Collectors.toMap(
				                                  line -> line.split(",")[0].trim(),
				                                  line -> Long.parseLong(line.split(",")[1].trim())));
		assert(maps.compareSize(matches, Maps.inputPattern3));
		assert(maps.compareContent(matches, Maps.inputPattern3));
	}
}
